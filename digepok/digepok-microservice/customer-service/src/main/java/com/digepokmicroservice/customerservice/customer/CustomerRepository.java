package com.digepokmicroservice.customerservice.customer;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.digepokmicroservice.customerservice.customer.model.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {
    
}
