package com.digepokmicroservice.customerservice.customer.model;

import java.io.Serializable;

import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.digepokmicroservice.customerservice.customer.model.dto.CustomerResponseDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Document(value = "customer")
@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Customer implements Serializable{
    @Id
    private String id;

    private String name;
    private String walletId;

    public CustomerResponseDto convertToDto(){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(this, CustomerResponseDto.class);
    }
}
