package com.digepokmicroservice.customerservice.customer;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.digepokmicroservice.customerservice.customer.model.Customer;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerRequestDto;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerResponseDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerRepository customerRepository;
    
    @GetMapping("/customers")
    public ResponseEntity<List<CustomerResponseDto>> fetchAll(){
        List<Customer> customers = this.customerRepository.findAll();

        return ResponseEntity.ok(customers.stream().map(Customer::convertToDto).toList());
    }

    @PostMapping("/customers")
    public ResponseEntity<CustomerResponseDto> add(@RequestBody CustomerRequestDto customerRequestDto){
        Customer customer = customerRequestDto.convertToEntity();
        Customer savedCustomer = this.customerRepository.save(customer);

        return ResponseEntity.status(HttpStatus.CREATED).body(savedCustomer.convertToDto());
    }
}
