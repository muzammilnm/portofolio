package com.digepokmicroservice.customerservice.wallet;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Document(value = "wallet")
@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Wallet implements Serializable{
    @Id
    private String id;
    private Double balance;
    private String cashtag;
    private Integer customerId;
}
