package com.digepokmicroservice.customerservice.customer.model.dto;

import org.modelmapper.ModelMapper;

import com.digepokmicroservice.customerservice.customer.model.Customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequestDto {
    private String name;

    public Customer convertToEntity(){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(this, Customer.class);
    }
}
