package com.digepokmicroservice.customerservice;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.digepokmicroservice.customerservice.customer.CustomerRepository;
import com.digepokmicroservice.customerservice.customer.model.Customer;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerRequestDto;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerResponseDto;
import com.digepokmicroservice.customerservice.wallet.Wallet;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureDataMongo
@AutoConfigureMockMvc
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Test
    void fetchAll_ShouldReturnAllCustomer_whenInvoked() throws Exception{
        Wallet wallet = Wallet.builder().balance(1000.0).cashtag("nikitaputri").build();
        Customer customer = Customer.builder().name("Nikita").walletId(wallet.getId()).build();
        this.customerRepository.save(customer);
        List<Customer> customers = this.customerRepository.findAll();
        List<CustomerResponseDto> expectedResult = customers.stream().map(Customer::convertToDto).toList();
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/customers"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();
        String responseString = result.getResponse().getContentAsString();

        List<CustomerResponseDto> actualResult = this.objectMapper.readerForListOf(
            CustomerResponseDto.class).readValue(responseString);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void add_shouldAddNewCustomerAndReturnNewCustomerWithNameNikita_whenGivenCustomerNameIsNikita() throws Exception{
        CustomerRequestDto customerRequestDto = CustomerRequestDto.builder().name("muzammil").build();
        String reguestJson = this.objectMapper.writeValueAsString(customerRequestDto);
        RequestBuilder builder = MockMvcRequestBuilders.post("/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(reguestJson)
            .accept(MediaType.APPLICATION_JSON);
        MvcResult result = this.mockMvc.perform(builder)
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andReturn();
        String respoString = result.getResponse().getContentAsString();

        CustomerResponseDto actualResult = this.objectMapper.readValue(respoString, CustomerResponseDto.class);
        Customer expectedCustomer = this.customerRepository.findById(actualResult.getId()).get();
        CustomerResponseDto expectedResult = expectedCustomer.convertToDto();

        Assertions.assertEquals(expectedResult, actualResult);
        
    }
}
