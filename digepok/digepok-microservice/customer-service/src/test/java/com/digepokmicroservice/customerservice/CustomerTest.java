package com.digepokmicroservice.customerservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.digepokmicroservice.customerservice.customer.model.Customer;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerResponseDto;

public class CustomerTest {
    
    @Test
    void convertToDto_shouldReturnCustomerDtoWithSameAttribute_whenConvertCustomerToCustomerDto(){
        Customer customer = Customer.builder().name("Muzammil").build();
        CustomerResponseDto expectedResult = CustomerResponseDto.builder().id(customer.getId()).name("Muzammil").build();

        CustomerResponseDto actualResult = customer.convertToDto();

        Assertions.assertEquals(expectedResult, actualResult);
    }
}
