package com.digepokmicroservice.customerservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.digepokmicroservice.customerservice.customer.model.Customer;
import com.digepokmicroservice.customerservice.customer.model.dto.CustomerRequestDto;

public class CustomerRequestDtoTest {
    @Test
    void convertToEntity_shouldReturnCustomerWithSameAttributes_whenConvertCustomerRequestDtoToCustomer(){
        Customer actualResult = Customer.builder().name("nikita").build();
        CustomerRequestDto customerRequestDto = CustomerRequestDto.builder().name("nikita").build();

        Customer expectedResult = customerRequestDto.convertToEntity();

        Assertions.assertEquals(expectedResult, actualResult);
    }
}
